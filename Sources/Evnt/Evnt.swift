import Foundation

// Event represents a change in the application state
struct Event {
    var body: String
    var date: Date

    init(_ _body: String) {
        body = _body
        date = Date()
    }
}

protocol Chain {
    // func sync()
    func log(_ event: Event) throws
}


