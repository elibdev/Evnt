import XCTest
@testable import Evnt

final class EvntTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        let e = Event("ok")
        XCTAssertEqual(e.body, "ok")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
