import XCTest

import evntTests

var tests = [XCTestCaseEntry]()
tests += evntTests.allTests()
XCTMain(tests)