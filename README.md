# Evnt

Evnt is an experimental library for storing event-based data.

The main abstraction is a generic chain of events that is easily synchronized.

## Goals

- develop a flexible method of modeling persistent data with events
- facilitate easy synchronization and deterministic merges

## Architecture

- `store`: content-addressed data store
- `feed`: sequence of events constructed from links in store

## Data Types

- content
- link

## Using Evnt in an Application

Here is one idea for how Evnt can be integrated into applications:

- `event` object is extended in swift code to be application-specific
- applications include as library or binary dependency
- apps communicate via ZeroMQ to append to or read from the event feed

Applications can perform the following actions:

- send an event to be stored
- stream a requested number of events from a given starting hash or event ID
- get metadata about feed (count, etc.)
- subscribe to updates on new events

Event schemas should be connected with an incremental version number.
A client should first confirm the event schema number when opening. 

Since the schemas are really the main thing we need to include,
maybe it would be better to make the schemas language-agnostic
using something like Protocol Buffers. 


