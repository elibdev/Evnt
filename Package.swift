// swift-tools-version:4.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Evnt",
    products: [
        .library(
            name: "Evnt",
            targets: ["Evnt"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "Evnt",
            dependencies: []),
        .testTarget(
            name: "EvntTests",
            dependencies: ["Evnt"]),
    ]
)
